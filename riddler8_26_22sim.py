import numpy
import matplotlib.pyplot as plt

def groups(cars,level):
    current_min=100
    new_group=0
    second_lane_cars=[]
    for c in range(len(cars)):
        if cars[c]<current_min:
            new_group+=1
            current_min=cars[c]
        elif level==1:
            second_lane_cars.append(cars[c])
    if level==1:
        second_lane_groups=groups(second_lane_cars,2)
    else:
        second_lane_groups=0
    return new_group+second_lane_groups

Ncars=[x for x in range(1,1000,10)]
result=[]

for N in Ncars:
    avegroups=0
    for n in range(1000):
        cars = numpy.random.uniform(0, 1, N)
        avegroups+=groups(cars,1)
    result.append(avegroups/1000)
    
Lower_limit=[]
for N in Ncars:
    s=0
    for n in range(1,N+1):
        s+=1.0/n*2
    Lower_limit.append(s)

print("cars: ",Ncars)
print("total groups: ",result)
plt.plot(Ncars,result,label="actual")
plt.plot(Ncars,Lower_limit,label="lower limit")
plt.legend(loc="upper left")
plt.show()

"""Sample Output:
cars:  [1, 11, 21, 31, 41, 51, 61, 71, 81, 91, 101, 111, 121, 131, 141, 151, 161, 171, 181, 191, 201, 211, 221, 231, 241, 251, 261, 271, 281, 291, 301, 311, 321, 331, 341, 351, 361, 371, 381, 391, 401, 411, 421, 431, 441, 451, 461, 471, 481, 491, 501, 511, 521, 531, 541, 551, 561, 571, 581, 591, 601, 611, 621, 631, 641, 651, 661, 671, 681, 691, 701, 711, 721, 731, 741, 751, 761, 771, 781, 791, 801, 811, 821, 831, 841, 851, 861, 871, 881, 891, 901, 911, 921, 931, 941, 951, 961, 971, 981, 991]
total groups:  [1.0, 6.143, 7.767, 8.808, 9.392, 10.081, 10.49, 10.943, 11.505, 11.741, 12.096, 12.228, 12.344, 12.767, 12.991, 13.045, 13.215, 13.275, 13.497, 13.615, 13.898, 13.944, 14.066, 14.194, 14.439, 14.408, 14.361, 14.442, 14.612, 14.777, 14.897, 14.931, 14.961, 15.248, 15.303, 15.165, 15.381, 15.604, 15.481, 15.763, 15.622, 15.718, 15.814, 15.871, 16.025, 16.096, 16.277, 16.044, 16.281, 16.45, 16.334, 16.405, 16.503, 16.544, 16.808, 16.563, 16.691, 16.53, 16.573, 17.016, 16.835, 16.822, 16.822, 16.946, 17.005, 16.962, 16.975, 17.279, 17.235, 17.086, 17.134, 17.19, 17.149, 17.409, 17.457, 17.414, 17.452, 17.634, 17.822, 17.563, 17.464, 17.486, 17.642, 17.46, 17.74, 17.764, 17.503, 17.792, 17.613, 17.835, 18.012, 18.022, 17.995, 18.1, 17.748, 17.876, 17.991, 18.079, 18.151, 18.234]
Plot link: https://bitbucket.org/pji2/riddler/src/master/riddler8_26_22image.png
"""