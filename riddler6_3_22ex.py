import itertools

def operations(N):
    arr=[]
    for b in range(2**N):
        rep=bin(b)
        rep=rep[2:]
        string='0'*(N-len(rep))
        string+=rep
        arr.append(string)
    return arr

N=4
countable=[]
for n in range(1,N+1):
    for comb in list(itertools.combinations([x+1 for x in range(N)],n)):
        for p in list(itertools.permutations(comb)):
            ops=operations(n-1)
            for o in ops:
                c=p[0]
                for i in range(1,n):
                    if o[i-1]=="0":
                        c+=p[i]
                    else:
                        c*=p[i]
                countable.append(c)
        
print(sorted(set(countable)))

"""Output:
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 28, 30, 32, 36]
"""