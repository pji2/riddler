import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def gen_next_step(x):
    if x<0.2:
        return random.random()*(x+0.2)
    elif x>0.8:
        return random.random()*(1-(x-0.2))+x-0.2
    else:
        return random.random()*0.4+x-0.2
    
x=0.5
arr=[0.5]
for i in range(1000000):
    x=gen_next_step(x)
    arr.append(x)

#histogram
bins=1000
p=plt.hist(arr,bins, range=None, density=True)
plt.show()
prob_density=p[0]

#average of maximum values
avemax=0
for i in range(bins):
    if i>.2*bins and i<.8*bins:
        avemax+=prob_density[i]/(.6*bins)

#find the minimum value by line-fitting to the intercept
xs=[i/float(bins) for i in range(int(bins/5))]
ys=prob_density[:int(bins/5)]
m=np.polyfit(xs,ys,1)
print(avemax,m[1])
print("ratio",avemax/m[1])

"""Output:
1.1103017105883979 0.5517750964541906
ratio 2.012235995650045
"""