light_arr=[[1,1,1,0,1,1,1],[0,0,1,0,0,1,0],[1,0,1,1,1,0,1],[1,0,1,1,0,1,1],[0,1,1,1,0,1,0],[1,1,0,1,0,1,1],[1,1,0,1,1,1,1],\
[1,0,1,0,0,1,0],[1,1,1,1,1,1,1],[1,1,1,1,0,1,0]]

def result(d1,d2):
    return [sum(x) for x in zip(light_arr[d1],light_arr[d2])]
    
lights=dict()
for x in range(100):
    res=result(int(x/10),x%10)
    res=tuple(res)
    if res not in lights:
        lights[res]=[x]
    else:
        lights[res].append(x)

for l in lights:
    if len(lights[l])>2:
        print(l,lights[l])

"""Output:
19, 47, 74, 91
"""