import random
import numpy as np
import csv

def csvread():
    strats=[[10,10,10,10,10,10,10,10,10,10]]
    with open("castle-solutions-4-valid.csv") as csvfile:
        reader=csv.reader(csvfile)
        for row in reader:
            l=[int(r) for r in row]
            strats=np.vstack([strats,l])
    return strats

#>27.5 points to win
def getpoints(a,b):
    greater=np.where((a-b)>0)[0]
    lesser=np.where((a-b)<0)[0]
    equal=np.where((a-b)==0)[0]
    return sum(greater+1)+sum(equal+1)/2.0

def battle(allstrats,strat):
    Record=0
    for i in range(len(allstrats)):
        wins=0
        b=allstrats[i]
        points=getpoints(strat,b)
        if points>27.5:
            wins=1
        elif points==27.5:
            wins=0.5
        Record+=wins
    return Record

def get_top(strats):
    N=len(strats)
    ranks=[]
    for s in strats:
        r=battle(strats,s)
        ranks.append(r)
    r=np.array(ranks)
    sorted_index_arr=np.argsort(r)
    strats_sorted=strats[sorted_index_arr]
    top_strats=strats_sorted[-int(N*.1):][:]
    return top_strats

def alter(strat):
    n=len(strat)
    for ni in range(1):
        i=random.randint(0,n-1)
        j=random.randint(0,n-1)
        while i==j or strat[j]==0:
            i=random.randint(0,n-1)
            j=random.randint(0,n-1)
        strat[i]+=1
        strat[j]-=1
    return strat
    
def main():
    allstratsorig=csvread()
    for n in range(5):
        top_strats=get_top(allstratsorig)
        #print(len(allstratsorig))
        for t in top_strats:
            for t_i in range(1):
                new_strat=alter(t)
                #print("before",allstratsorig)
                allstratsorig=np.vstack([allstratsorig,new_strat])
                #print("after",allstratsorig)
    top=get_top(allstratsorig)
    print(top[0:10][:])

main()
"""
Sample Output:
[[ 3  3  5 11 13  2 23  1  3 36]
 [ 0  1  0 15 17  2  4  2 35 24]
 [ 0  1  2 19  5 22  5  5  7 34]
 [ 1  1  1 11 14  2  3  2 30 35]
 [ 4  3  4  9 13  2 23  2  4 36]
 [ 0  0  8 11 13 24  3  2  6 33]
 [ 4  3  5 10 12  2 23  2  3 36]
 [ 0  2  1 12 14  2  3  2 30 34]
 [ 0  2  0 12 14  2  3  2 30 35]
 [ 0  0  0 12 19  4  2  2 35 26]]
"""