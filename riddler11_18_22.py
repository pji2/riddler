def convolve(D1,D2):
    minval=D1.index(0)+1 + D2.index(0)+1
    maxval=len(D1)-1+len(D2)-1
    values=[i for i in range(minval,maxval+1)]
    valueprobs=[0 for i in range(maxval+1)]
    for v_i in range(len(values)):
        v=values[v_i]
        p=0
        for d1_i in range(len(D1)):
            d2_v=v-d1_i
            if d2_v>=0 and d2_v<len(D2):
                p+=D1[d1_i]*D2[d2_v]
        valueprobs[v]=p
    return valueprobs
    
def find_num_max(dist):
    maxv=0
    maxi=0
    for d in dist:
        if d>maxv:
            maxv=d
            maxi=dist.index(d)
    c=0
    for d in dist:
        if abs(d-maxv)<.000001: 
            c+=1
    return c,maxi


def numberToBase(n, b, places):
    if n == 0:
        return "0"*places
    digits = []
    while n:
        digits.append(int(n % b))
        n //= b
    res=digits[::-1]
    res_str=""
    for r in res:
        res_str+=str(r)
    while len(res_str)<places:
        res_str="0"+res_str
    return res_str
    
def convolveDice(code):
    code_arr=[]
    for c in code:
        code_arr.append(int(c))
    #print(code_arr)
    for c in range(len(code_arr)-1): #sequence must be increasing
        if code_arr[c]>code_arr[c+1]:
            return ([0,0],(0,0))
    dicekey={0:[0,1/4,1/4,1/4,1/4], 1:[0,1/6,1/6,1/6,1/6,1/6,1/6], 2:[0,1/8,1/8,1/8,1/8,1/8,1/8,1/8,1/8]\
    , 3:[0,1/12,1/12,1/12,1/12,1/12,1/12,1/12,1/12,1/12,1/12,1/12,1/12], 4:[0,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20,1/20]}
    dice_int=convolve(dicekey[code_arr[0]],dicekey[code_arr[1]])
    for i in range(2,len(code_arr)):
        dice_int=convolve(dice_int,dicekey[code_arr[i]])
    res=find_num_max(dice_int)
    if res[0]==1:
        return (code,res)
    return ([0,0],(0,0))
    
#2 dice 
poss_values=[]
for i in range(5**2):
    dicecode=numberToBase(i,5,2)
    res=convolveDice(dicecode)
    if res[1][0]==1:
        #print(res)
        poss_values.append(res[1][1])
#4 dice
for i in range(5**4):
    dicecode=numberToBase(i,5,4)
    res=convolveDice(dicecode)
    if res[1][0]==1:
        #print(res)
        poss_values.append(res[1][1])
#6 dice
for i in range(5**6):
    dicecode=numberToBase(i,5,6)
    res=convolveDice(dicecode)
    if res[1][0]==1:
        #print(res)
        poss_values.append(res[1][1])
#8 dice
for i in range(5**8):
    dicecode=numberToBase(i,5,8)
    res=convolveDice(dicecode)
    if res[1][0]==1:
        #print(res)
        poss_values.append(res[1][1])
#10 dice
for i in range(5**10):
    dicecode=numberToBase(i,5,10)
    res=convolveDice(dicecode)
    if res[1][0]==1:
        #print(res)
        poss_values.append(res[1][1])
poss_values.sort()
print("possible most likely sums:", set(poss_values))

"""Output:
possible most likely sums: {5, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 97, 98, 99, 101, 105}
"""
#Not possible most likely sums: 1,2,3,4,6,8