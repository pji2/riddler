def pad(b):
    l=len(b)
    s='0'*(6-l)+str(b)
    return s 

def ternary (n):
    if n == 0:
        return '0'
    nums = []
    while n:
        n, r = divmod(n, 3)
        nums.append(str(r))
    return ''.join(reversed(nums))
    
matchdict={0:(0,1), 1:(0,2), 2:(0,3), 3:(1,3), 4:(1,2), 5:(2,3)}

uniquetuples=set()
for gs in range(3**6):
    games=pad(ternary(gs))
    #print(games)
    points=[0,0,0,0]
    for g_i in range(len(games)):
        player1=matchdict[g_i][0]
        player2=matchdict[g_i][1]
        if games[g_i]=="0":
            points[player1]+=0 
            points[player2]+=3
        elif games[g_i]=="1":
            points[player1]+=1 
            points[player2]+=1 
        else:
            points[player1]+=3 
            points[player2]+=0
    if len(points)==len(set(points)):
        points.sort(reverse=True)
        ps=tuple(points)
        uniquetuples.add(ps)
        
print(uniquetuples)


###Output:
#{(9, 4, 2, 1), (9, 6, 3, 0), (9, 4, 3, 1), (7, 4, 3, 2), (7, 5, 4, 0), (7, 4, 3, 1), (6, 5, 4, 1), (5, 4, 3, 2), (7, 6, 2, 1), (7, 6, 4, 0), (7, 6, 3, 1), (7, 5, 2, 1), (7, 5, 3, 1)}
