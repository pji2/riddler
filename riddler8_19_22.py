import operator as op
from functools import reduce

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom  # or / in Python 2
    
def TwoFull_Prob(a,b):
    return ncr(a,2)/ncr(a+b,2)

def OneFull1Half_Prob(a,b):
    return a*b/ncr(a+b,2)
    
def TwoHalfsPlusFull(a,b):
    p1=ncr(b,2)/ncr(a+b,2)
    p2=a/(a+b-2)
    return p1*p2

def TwoHalfsPlusHalf(a,b):
    p1=ncr(b,2)/ncr(a+b,2)
    p2=(b-2)/(a+b-2)
    return p1*p2


def p(i,a,b):
    if i==0 and a==15 and b==0:
        return 1
    elif i==0:
        return 0
    else:
        TotalProb=0
        if b-1>=0:
            TotalProb+=p(i-1,a+2,b-1)*TwoFull_Prob(a+2,b-1)
        if b+1>=2:
            TotalProb+=p(i-1,a+1,b+1)*TwoHalfsPlusFull(a+1,b+1)
        TotalProb+=p(i-1,a+1,b+1)*OneFull1Half_Prob(a+1,b+1)
        TotalProb+=p(i-1,a,b+3)*TwoHalfsPlusHalf(a,b+3)
    return TotalProb
print(p(9,1,1))

#Output:
#0.7901197017268445   