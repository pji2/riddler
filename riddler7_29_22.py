from scipy.integrate import dblquad
from numpy import sin, cos, pi, exp, sqrt, inf

maxprob=0
max_t=0
max_s=0
ts=[]
probs=[]
for t_ in range(100,142):
    t = t_/100.0
    s = sqrt(4-t**2)
    o1=dblquad(lambda y,x: exp(-(x**2+y**2)/2), -1, 1, lambda y:-sqrt(1-(y)**2)+t, lambda y:sqrt(1-(y)**2)+t)
    o2=dblquad(lambda y,x: exp(-(x**2+y**2)/2), s-1, s+1, lambda y:-sqrt(1-(y-s)**2), lambda y:sqrt(1-(y-s)**2))
    prob=((2*o1[0]+2*o2[0])/(2*pi))
    ts.append(t)
    probs.append(prob)
    if prob>maxprob:
        maxprob=prob
        max_t = t
        max_s = s
        
print(maxprob, max_t, max_s)

"""Output:
0.7778913868650846 1.0 1.7320508075688772
"""
