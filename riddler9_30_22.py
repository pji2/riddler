#Riddler 9-30-22
import math

R=1

def IntersectionPoint(L1_s,L1_end,L2_s,L2_end):
    #https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
    x1=L1_s[0];y1=L1_s[1]
    x2=L1_end[0];y2=L1_end[1]
    x3=L2_s[0];y3=L2_s[1]
    x4=L2_end[0];y4=L2_end[1]
    t=((x1-x3)*(y3-y4)-(y1-y3)*(x3-x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
    u=((x1-x3)*(y1-y2)-(y1-y2)*(x1-x2))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
    if t>=0 and t<=1 and u>=0 and u<=1:
        return (x1+t*(x2-x1),y1+t*(y2-y1))
    return -1
        
def dist(P,Q):
    return math.sqrt((P[0]-Q[0])**2 + (P[1]-Q[1])**2)
    
def I_C_Same_Side(I,C,P1,P2):
    #https://math.stackexchange.com/questions/162728/how-to-determine-if-2-points-are-on-opposite-sides-of-a-line#:~:text=It%20should%20be%20of%20the,0%2C%20or%20visa%2Dversa.
    ax=I[0];ay=I[1]
    bx=0;by=0 
    x1=P1[0];y1=P1[1]
    x2=P2[0];y2=P2[1]
    if ((y1-y2)*(ax-x1)+(x2-x1)*(ay-y1))*((y1-y2)*(bx-x1)+(x2-x1)*(by-y1))<0:
        return False
    return True
    
def TriangleArea(P1,P2,P3):
    #get Triangle Area
    a=dist(P1,P2);b=dist(P1,P3);c=dist(P2,P3)
    s=(a+b+c)/2.0 
    return math.sqrt(s*(s-a)*(s-b)*(s-c))
    
def SectorArea(P1,P2):
    a=dist(P1,P2)
    thetaC=2*math.asin(a/(2*R))
    return 0.5*R**2*thetaC

def Area(P1,P2,I):
    C=(0,0)
    if I_C_Same_Side(I,C,P1,P2):
        #get Triangle Area
        TA=TriangleArea(P1,P2,I)
        #get Sector slice
        SAC=SectorArea(P1,P2)-TriangleArea(P1,P2,C)
        return TA+SAC
    else:
        A=math.pi*R**2-SectorArea(P1,P2)+TriangleArea(P1,P2,I)+TriangleArea(P1,P2,C)
        return A
        
def ratios(As):
    r1=As[1]/As[0];r2=As[2]/As[0];r3=As[3]/As[0]
    dev=(r1-2)**2+(r2-3)**2+(r3-4)**2 
    return dev

incs=50.0
mindev=100
minAreas=[0,0,0,0]
Lines=((1,0),(0,0),(0,0),(0,0)) #Line1start, Line1end, Line2start, Line2end
angles=[0,0,0]
for theta1_ in range(1,int(math.pi*incs)+1):
    theta1=theta1_/incs
    Line1_end=(R*math.cos(theta1),R*math.sin(theta1))
    for theta2_ in range(2,theta1_):
        theta2=theta2_/incs
        Line2_start=(R*math.cos(theta2),R*math.sin(theta2))
        for theta3_ in range(theta1_+1,int(2*math.pi*incs)):
            theta3=theta3_/incs
            Line2_end=(R*math.cos(theta3),R*math.sin(theta3))
            I=IntersectionPoint((1,0),Line1_end,Line2_start,Line2_end)
            if I==-1:
                continue
            Area_1=Area((1,0),Line2_start,I)
            Area_2=Area(Line2_start,Line1_end,I)
            Area_3=Area(Line1_end,Line2_end,I)
            Area_4=Area(Line2_end,(1,0),I)
            As=[Area_1,Area_2,Area_3,Area_4]
            As_sort=sorted(As)
            dev=ratios(As_sort)
            if dev<mindev:
                mindev=dev
                minAreas=As_sort
                Lines=((1,0),Line1_end,Line2_start,Line2_end)
                angles=[theta1,theta2,theta3]
#print(mindev)
#print(minAreas,sum(minAreas))
print("Angles: ",[a*180/math.pi for a in angles])
print("Line 1 endpoints: ",Lines[0],Lines[1])
print("Line 2 endpoints: ",Lines[2],Lines[3])
print("ratios: ",minAreas[1]/minAreas[0],minAreas[2]/minAreas[0],minAreas[3]/minAreas[0])

#Plotting:
import matplotlib.pyplot as plt
data=[(Lines[0][0],Lines[1][0]),(Lines[0][1],Lines[1][1]),'r',(Lines[2][0],Lines[3][0]),(Lines[2][1],Lines[3][1]),'b']
plt.plot(*data)
circle1 = plt.Circle((0, 0), 1, color='black', fill=False)
ax = plt.gca()
ax.add_patch(circle1)
plt.gca().set_aspect('equal')
plt.plot(0,0,'o',color='black')
plt.show()

"""Output
Angles:  [179.90874767107852, 118.02930579694959, 316.2727029122144]
Line 1 endpoints:  (1, 0) (-0.9999987317275395, 0.0015926529164868282)
Line 2 endpoints:  (-0.4699231137276022, 0.8827073508159741) (0.7226379108705916, -0.6912267715971271)
ratios:  2.0032963840230553 3.0084353122454193 4.0219049464647485
*Image at https://bitbucket.org/pji2/riddler/src/master/riddler9_30_22image.png
"""
