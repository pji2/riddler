import math
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
R=1
K=2
def equations(var,dist_t,dt,x0,y0):
    x1,y1=var
    eq1=(x1-math.sqrt(2)/2*dist_t)**2+(y1-math.sqrt(2)/2*dist_t)**2-R**2
    eq2=(x1-x0)**2+(y1-y0)**2-(K*dt)**2
    return [eq1,eq2]
x0=R*math.sqrt(2)/2
y0=R*math.sqrt(2)/2
dt=.00001
i=0
dist_t=dt*i
ycoord=dist_t*math.sqrt(2)/2
pathx=[]
pathy=[]
mex=[]
mey=[]
while ycoord<y0:
    i+=1
    dist_t=dt*i
    ycoord=dist_t*math.sqrt(2)/2
    res=fsolve(equations,(x0+.001,y0-.001),args=(dist_t,dt,x0,y0))
    x0=res[0]
    y0=res[1]
    #print(y0,ycoord)
    mex.append(ycoord);mey.append(ycoord)
    pathx.append(x0);pathy.append(y0)
print(x0)
plt.plot(mex,mey)
plt.plot(pathx,pathy)
plt.show()

#Output:
#K=2: 2.5220852204189543
#K=3: 2.3453226672286216