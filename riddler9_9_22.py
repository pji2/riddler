###Google doc with sketch, code, and solution image: https://docs.google.com/document/d/1oP5blduYnaADEINtVl9JhnC69DqEpT-djNeEPLjctTE/edit
from sympy import *

def FindCenterLastCircle(t,u,r):
    x, y = symbols('x y')
    eq1 = ((x-u)**2+(y+0.5)**2)-r**2
    eq2 = ((x-.5)**2+(y-(.5-t))**2)-r**2
    sol=solve((eq1,eq2), (x, y))
    return sol

def dist(p1,p2):
    return sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)

"""r=3.264
t=sqrt((2*r)**2-.5**2)
if r>1-t-r:
        u=sqrt(r**2-(1-t-r)**2)
        sol = FindCenterLastCircle(t,u,r)
        if sol[0][0].is_real and dist(sol[1],(.5,-.5))<r:
            print(r,t,u)
            print(sol)
"""
            
for t_ in range(1,50*50):
    t = t_/(100*50)
    r=sqrt(0.5**2+t**2)/2
    if r>1-t-r:
        u=sqrt(r**2-(1-t-r)**2)
        sol = FindCenterLastCircle(t,u,r)
        if sol[0][0].is_real and dist(sol[1],(.5,-.5))<r:
            print(r,t,u)
            #print(sol)
            circle3center=sol[1]
            break
            
print("Circle radius: ",r)
print("Circle center coordinates:")
print((.25,.5-t/2))
print((0,-.5+(1-t-r)))
print(circle3center)
print((-.25,.5-t/2))
print((-circle3center[0],circle3center[1]))

"""Output:
0.326175182992207 0.419 0.203603935710843
Circle radius:  0.326175182992207
Circle center coordinates:
(0.25, 0.2905)
(0, -0.245175182992207)
(0.357245460220856, -0.212276987458008)
(-0.25, 0.2905)
(-0.357245460220856, -0.212276987458008)
"""

#Plotting code:
import matplotlib.pyplot as plt
 
figure, axes = plt.subplots()
C1 = plt.Circle((.25,.5-t/2), r, fill=False)
C2 = plt.Circle((0,-.5+(1-t-r)), r, fill=False)
C3 = plt.Circle(circle3center, r, fill=False)

C1_r = plt.Circle((-.25,.5-t/2), r, fill=False)
C3_r = plt.Circle((-circle3center[0],circle3center[1]), r, fill=False)

UnitSquare = plt.Rectangle((-.5,-.5), 1, 1, fill=False)
 
axes.add_artist(C1)
axes.add_artist(C2)
axes.add_artist(C3)
axes.add_artist(C1_r)
axes.add_artist(C3_r)
axes.add_artist(UnitSquare)
plt.xlim([-1,1])
plt.ylim([-1,1])
plt.gca().set_aspect('equal', adjustable='box')
plt.show()
