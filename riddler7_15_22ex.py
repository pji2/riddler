from itertools import permutations

def col_permute(old,order):
    #print(old,order)
    new=["","","","",""]
    for i in range(5):
        new[i]=old[order[i]]
    return new

def row_permute(old,order):
    #print("row",old,order)
    new=[old[0],"","","",""]
    for i in range(1,5):
        #print(new,i,order,i)
        new[i]=old[order[i-1]]
    return new

def genmatrix(reducedLS,column_put,row_put):
    newLS=[["" for x in range(5)] for y in range(5)]
    for r_i in range(len(reducedLS)): #loop over each row
        newLS[r_i]=col_permute(reducedLS[r_i],column_put)
    #print("newLS",newLS,column_put)
    #then permute the rows
    new=row_permute(newLS,row_put)
    #print("newLSS",new,row_put)
    return new
    

num_LSs=0
num_valid=0
num_lines=0
twooffour=0
f = open('reduced_latin_squares_order5.txt')
for square in f:
    num_lines+=1
    square=square.strip()
    square=square.replace(" ","")
    square=square+"9"
    #print(square,len(square))
    matrix=[]
    row=[]
    for s_i in range(len(square)):
        if s_i%5==0 and s_i>0:
            matrix.append(row)
            row=[int(square[s_i])]
        else:
            row.append(int(square[s_i]))
    for column_put in list(permutations([0,1,2,3,4])):
        for row_put in list(permutations([1,2,3,4])):
            m=genmatrix(matrix,column_put,row_put)
            num_LSs+=1
            if m[0][0]!=0 and m[4][0]!=0 and m[0][4]!=0 and m[4][4]!=0:
                num_valid+=1
            if m[0][0]==m[4][0] or m[0][0]==m[4][4] or m[0][0]==m[0][4] or m[0][4]==m[4][4] or m[0][4]==m[4][0] or m[4][4]==m[4][0]:
                twooffour+=1
print(num_lines, num_LSs, num_valid, twooffour)
print(num_valid/num_LSs, twooffour/num_LSs)

"""Output:
56 161280 48384 74880
0.3 0.4642857142857143
"""