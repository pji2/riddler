import random

def admissible(Nseq):
    b=sorted(Nseq)
    for i in range(10):
        if b[i]<i:
            return False
    return True
    
tower_height=10
c=0
for n in range(10000000):
    sequence=[]
    for i in range(tower_height):
        sequence.append(random.randint(0,9))
    if admissible(sequence):
        c+=1
    #else:
     #   print(sequence)
print(c/10000000)

"""sample output:
0.2357857
"""