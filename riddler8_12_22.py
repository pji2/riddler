import random
def simgame(x,a,b):
    ac=a
    bc=b
    if x<=0.000001:
        return 0
    elif x>=1-.000001:
        return 1
    if 1-x<b:
        b=1-x
    if a>x:
        a=x
    p=.9*a/(a+b)
    pi=random.random()
    if pi<p:
        x+=b
    else:
        x-=a
    return simgame(x,ac,bc)

for a in range(5,100,5):
    a_=a/100
    print("A=",a_,)
    print("-----------------------------")
    for b in range(5,100,5):
        wins=0
        b_=b/100
        for n in range(1000):
            wins+=simgame(.4,a_,b_)
        print("B-A=",b_,"win_prob:",wins/1000)
    print("")

"""Output:
A= 0.05
-----------------------------
B-A= 0.05 win_prob: 0.0713
B-A= 0.1 win_prob: 0.1914
B-A= 0.15 win_prob: 0.2392
B-A= 0.2 win_prob: 0.28
B-A= 0.25 win_prob: 0.2971
B-A= 0.3 win_prob: 0.3154
B-A= 0.35 win_prob: 0.3284
B-A= 0.4 win_prob: 0.3337
B-A= 0.45 win_prob: 0.3542
B-A= 0.5 win_prob: 0.3516
B-A= 0.55 win_prob: 0.3494
B-A= 0.6 win_prob: 0.3607
B-A= 0.65 win_prob: 0.3578
B-A= 0.7 win_prob: 0.363
B-A= 0.75 win_prob: 0.3638
B-A= 0.8 win_prob: 0.3603
B-A= 0.85 win_prob: 0.3692
B-A= 0.9 win_prob: 0.3622
B-A= 0.95 win_prob: 0.3723

A= 0.1
-----------------------------
B-A= 0.05 win_prob: 0.0722
B-A= 0.1 win_prob: 0.1886
B-A= 0.15 win_prob: 0.2513
B-A= 0.2 win_prob: 0.284
B-A= 0.25 win_prob: 0.3062
B-A= 0.3 win_prob: 0.3238
B-A= 0.35 win_prob: 0.3241
B-A= 0.4 win_prob: 0.336
B-A= 0.45 win_prob: 0.3517
B-A= 0.5 win_prob: 0.3493
B-A= 0.55 win_prob: 0.3498
B-A= 0.6 win_prob: 0.3544
B-A= 0.65 win_prob: 0.3528
B-A= 0.7 win_prob: 0.3662
B-A= 0.75 win_prob: 0.3648
B-A= 0.8 win_prob: 0.3635
B-A= 0.85 win_prob: 0.3617
B-A= 0.9 win_prob: 0.3638
B-A= 0.95 win_prob: 0.3676

A= 0.15
-----------------------------
B-A= 0.05 win_prob: 0.0803
B-A= 0.1 win_prob: 0.1928
B-A= 0.15 win_prob: 0.2492
B-A= 0.2 win_prob: 0.277
B-A= 0.25 win_prob: 0.3053
B-A= 0.3 win_prob: 0.3104
B-A= 0.35 win_prob: 0.3327
B-A= 0.4 win_prob: 0.334
B-A= 0.45 win_prob: 0.3403
B-A= 0.5 win_prob: 0.3452
B-A= 0.55 win_prob: 0.3587
B-A= 0.6 win_prob: 0.36
B-A= 0.65 win_prob: 0.3511
B-A= 0.7 win_prob: 0.3645
B-A= 0.75 win_prob: 0.3582
B-A= 0.8 win_prob: 0.3676
B-A= 0.85 win_prob: 0.3617
B-A= 0.9 win_prob: 0.3638
B-A= 0.95 win_prob: 0.3722

A= 0.2
-----------------------------
B-A= 0.05 win_prob: 0.0834
B-A= 0.1 win_prob: 0.1926
B-A= 0.15 win_prob: 0.2497
B-A= 0.2 win_prob: 0.281
B-A= 0.25 win_prob: 0.31
B-A= 0.3 win_prob: 0.32
B-A= 0.35 win_prob: 0.3138
B-A= 0.4 win_prob: 0.333
B-A= 0.45 win_prob: 0.3352
B-A= 0.5 win_prob: 0.3394
B-A= 0.55 win_prob: 0.3417
B-A= 0.6 win_prob: 0.3564
B-A= 0.65 win_prob: 0.363
B-A= 0.7 win_prob: 0.3658
B-A= 0.75 win_prob: 0.3512
B-A= 0.8 win_prob: 0.3773
B-A= 0.85 win_prob: 0.3632
B-A= 0.9 win_prob: 0.3511
B-A= 0.95 win_prob: 0.3705

A= 0.25
-----------------------------
B-A= 0.05 win_prob: 0.0879
B-A= 0.1 win_prob: 0.1996
B-A= 0.15 win_prob: 0.2518
B-A= 0.2 win_prob: 0.2783
B-A= 0.25 win_prob: 0.2947
B-A= 0.3 win_prob: 0.3152
B-A= 0.35 win_prob: 0.3322
B-A= 0.4 win_prob: 0.3349
B-A= 0.45 win_prob: 0.3381
B-A= 0.5 win_prob: 0.3418
B-A= 0.55 win_prob: 0.3468
B-A= 0.6 win_prob: 0.3577
B-A= 0.65 win_prob: 0.3608
B-A= 0.7 win_prob: 0.356
B-A= 0.75 win_prob: 0.3623
B-A= 0.8 win_prob: 0.3615
B-A= 0.85 win_prob: 0.3623
B-A= 0.9 win_prob: 0.3608
B-A= 0.95 win_prob: 0.353

A= 0.3
-----------------------------
B-A= 0.05 win_prob: 0.0967
B-A= 0.1 win_prob: 0.1984
B-A= 0.15 win_prob: 0.2603
B-A= 0.2 win_prob: 0.2839
B-A= 0.25 win_prob: 0.294
B-A= 0.3 win_prob: 0.3203
B-A= 0.35 win_prob: 0.3233
B-A= 0.4 win_prob: 0.3366
B-A= 0.45 win_prob: 0.3402
B-A= 0.5 win_prob: 0.3388
B-A= 0.55 win_prob: 0.3468
B-A= 0.6 win_prob: 0.3584
B-A= 0.65 win_prob: 0.3537
B-A= 0.7 win_prob: 0.3602
B-A= 0.75 win_prob: 0.3561
B-A= 0.8 win_prob: 0.3668
B-A= 0.85 win_prob: 0.362
B-A= 0.9 win_prob: 0.354
B-A= 0.95 win_prob: 0.3657

A= 0.35
-----------------------------
B-A= 0.05 win_prob: 0.0941
B-A= 0.1 win_prob: 0.2026
B-A= 0.15 win_prob: 0.26
B-A= 0.2 win_prob: 0.2833
B-A= 0.25 win_prob: 0.2966
B-A= 0.3 win_prob: 0.3245
B-A= 0.35 win_prob: 0.3257
B-A= 0.4 win_prob: 0.3287
B-A= 0.45 win_prob: 0.3343
B-A= 0.5 win_prob: 0.3367
B-A= 0.55 win_prob: 0.3423
B-A= 0.6 win_prob: 0.3634
B-A= 0.65 win_prob: 0.3591
B-A= 0.7 win_prob: 0.36
B-A= 0.75 win_prob: 0.3563
B-A= 0.8 win_prob: 0.3498
B-A= 0.85 win_prob: 0.3665
B-A= 0.9 win_prob: 0.3568
B-A= 0.95 win_prob: 0.3568

A= 0.4
-----------------------------
B-A= 0.05 win_prob: 0.0968
B-A= 0.1 win_prob: 0.2097
B-A= 0.15 win_prob: 0.2584
B-A= 0.2 win_prob: 0.2913
B-A= 0.25 win_prob: 0.2971
B-A= 0.3 win_prob: 0.3175
B-A= 0.35 win_prob: 0.3259
B-A= 0.4 win_prob: 0.3241
B-A= 0.45 win_prob: 0.3285
B-A= 0.5 win_prob: 0.3432
B-A= 0.55 win_prob: 0.3413
B-A= 0.6 win_prob: 0.3635
B-A= 0.65 win_prob: 0.3577
B-A= 0.7 win_prob: 0.3657
B-A= 0.75 win_prob: 0.3634
B-A= 0.8 win_prob: 0.3648
B-A= 0.85 win_prob: 0.3493
B-A= 0.9 win_prob: 0.3602
B-A= 0.95 win_prob: 0.3579

A= 0.45
-----------------------------
B-A= 0.05 win_prob: 0.1014
B-A= 0.1 win_prob: 0.2042
B-A= 0.15 win_prob: 0.2614
B-A= 0.2 win_prob: 0.2948
B-A= 0.25 win_prob: 0.2967
B-A= 0.3 win_prob: 0.3251
B-A= 0.35 win_prob: 0.3183
B-A= 0.4 win_prob: 0.3306
B-A= 0.45 win_prob: 0.3361
B-A= 0.5 win_prob: 0.3347
B-A= 0.55 win_prob: 0.3456
B-A= 0.6 win_prob: 0.3553
B-A= 0.65 win_prob: 0.3577
B-A= 0.7 win_prob: 0.3629
B-A= 0.75 win_prob: 0.3597
B-A= 0.8 win_prob: 0.3638
B-A= 0.85 win_prob: 0.3633
B-A= 0.9 win_prob: 0.3583
B-A= 0.95 win_prob: 0.3589

A= 0.5
-----------------------------
B-A= 0.05 win_prob: 0.0978
B-A= 0.1 win_prob: 0.2102
B-A= 0.15 win_prob: 0.251
B-A= 0.2 win_prob: 0.2823
B-A= 0.25 win_prob: 0.2992
B-A= 0.3 win_prob: 0.3305
B-A= 0.35 win_prob: 0.3275
B-A= 0.4 win_prob: 0.3313
B-A= 0.45 win_prob: 0.32
B-A= 0.5 win_prob: 0.3276
B-A= 0.55 win_prob: 0.3414
B-A= 0.6 win_prob: 0.3587
B-A= 0.65 win_prob: 0.3604
B-A= 0.7 win_prob: 0.3682
B-A= 0.75 win_prob: 0.3552
B-A= 0.8 win_prob: 0.3497
B-A= 0.85 win_prob: 0.3657
B-A= 0.9 win_prob: 0.3603
B-A= 0.95 win_prob: 0.3652

A= 0.55
-----------------------------
B-A= 0.05 win_prob: 0.1065
B-A= 0.1 win_prob: 0.2048
B-A= 0.15 win_prob: 0.258
B-A= 0.2 win_prob: 0.2949
B-A= 0.25 win_prob: 0.2955
B-A= 0.3 win_prob: 0.3252
B-A= 0.35 win_prob: 0.315
B-A= 0.4 win_prob: 0.3235
B-A= 0.45 win_prob: 0.3298
B-A= 0.5 win_prob: 0.3304
B-A= 0.55 win_prob: 0.3262
B-A= 0.6 win_prob: 0.3612
B-A= 0.65 win_prob: 0.3521
B-A= 0.7 win_prob: 0.3614
B-A= 0.75 win_prob: 0.3584
B-A= 0.8 win_prob: 0.3549
B-A= 0.85 win_prob: 0.3642
B-A= 0.9 win_prob: 0.3568
B-A= 0.95 win_prob: 0.3605

A= 0.6
-----------------------------
B-A= 0.05 win_prob: 0.1086
B-A= 0.1 win_prob: 0.2113
B-A= 0.15 win_prob: 0.2555
B-A= 0.2 win_prob: 0.2926
B-A= 0.25 win_prob: 0.2932
B-A= 0.3 win_prob: 0.3279
B-A= 0.35 win_prob: 0.3288
B-A= 0.4 win_prob: 0.3273
B-A= 0.45 win_prob: 0.3415
B-A= 0.5 win_prob: 0.3328
B-A= 0.55 win_prob: 0.3376
B-A= 0.6 win_prob: 0.359
B-A= 0.65 win_prob: 0.3628
B-A= 0.7 win_prob: 0.3528
B-A= 0.75 win_prob: 0.3541
B-A= 0.8 win_prob: 0.361
B-A= 0.85 win_prob: 0.3579
B-A= 0.9 win_prob: 0.354
B-A= 0.95 win_prob: 0.3646

A= 0.65
-----------------------------
B-A= 0.05 win_prob: 0.114
B-A= 0.1 win_prob: 0.2137
B-A= 0.15 win_prob: 0.2605
B-A= 0.2 win_prob: 0.2832
B-A= 0.25 win_prob: 0.2954
B-A= 0.3 win_prob: 0.3215
B-A= 0.35 win_prob: 0.3264
B-A= 0.4 win_prob: 0.3268
B-A= 0.45 win_prob: 0.3233
B-A= 0.5 win_prob: 0.3329
B-A= 0.55 win_prob: 0.3326
B-A= 0.6 win_prob: 0.363
B-A= 0.65 win_prob: 0.3637
B-A= 0.7 win_prob: 0.3554
B-A= 0.75 win_prob: 0.3583
B-A= 0.8 win_prob: 0.3587
B-A= 0.85 win_prob: 0.3632
B-A= 0.9 win_prob: 0.3585
B-A= 0.95 win_prob: 0.364

A= 0.7
-----------------------------
B-A= 0.05 win_prob: 0.1101
B-A= 0.1 win_prob: 0.2074
B-A= 0.15 win_prob: 0.26
B-A= 0.2 win_prob: 0.2993
B-A= 0.25 win_prob: 0.2941
B-A= 0.3 win_prob: 0.3268
B-A= 0.35 win_prob: 0.3343
B-A= 0.4 win_prob: 0.3202
B-A= 0.45 win_prob: 0.3253
B-A= 0.5 win_prob: 0.3311
B-A= 0.55 win_prob: 0.3307
B-A= 0.6 win_prob: 0.3515
B-A= 0.65 win_prob: 0.3708
B-A= 0.7 win_prob: 0.3584
B-A= 0.75 win_prob: 0.3605
B-A= 0.8 win_prob: 0.3537
B-A= 0.85 win_prob: 0.3583
B-A= 0.9 win_prob: 0.3603
B-A= 0.95 win_prob: 0.3603

A= 0.75
-----------------------------
B-A= 0.05 win_prob: 0.1098
B-A= 0.1 win_prob: 0.2107
B-A= 0.15 win_prob: 0.2606
B-A= 0.2 win_prob: 0.2996
B-A= 0.25 win_prob: 0.2877
B-A= 0.3 win_prob: 0.3239
B-A= 0.35 win_prob: 0.33
B-A= 0.4 win_prob: 0.3268
B-A= 0.45 win_prob: 0.3249
B-A= 0.5 win_prob: 0.3324
B-A= 0.55 win_prob: 0.3326
B-A= 0.6 win_prob: 0.3609
B-A= 0.65 win_prob: 0.3529
B-A= 0.7 win_prob: 0.3688
B-A= 0.75 win_prob: 0.3595
B-A= 0.8 win_prob: 0.3589
B-A= 0.85 win_prob: 0.3539
B-A= 0.9 win_prob: 0.3551
B-A= 0.95 win_prob: 0.3696

A= 0.8
-----------------------------
B-A= 0.05 win_prob: 0.1132
B-A= 0.1 win_prob: 0.2139
B-A= 0.15 win_prob: 0.2645
B-A= 0.2 win_prob: 0.2887
B-A= 0.25 win_prob: 0.2899
B-A= 0.3 win_prob: 0.3226
B-A= 0.35 win_prob: 0.3233
B-A= 0.4 win_prob: 0.3169
B-A= 0.45 win_prob: 0.3152
B-A= 0.5 win_prob: 0.3358
B-A= 0.55 win_prob: 0.3338
B-A= 0.6 win_prob: 0.3626
B-A= 0.65 win_prob: 0.3625
B-A= 0.7 win_prob: 0.3528
B-A= 0.75 win_prob: 0.3604
B-A= 0.8 win_prob: 0.3623
B-A= 0.85 win_prob: 0.3654
B-A= 0.9 win_prob: 0.3583
B-A= 0.95 win_prob: 0.3519

A= 0.85
-----------------------------
B-A= 0.05 win_prob: 0.114
B-A= 0.1 win_prob: 0.2222
B-A= 0.15 win_prob: 0.2569
B-A= 0.2 win_prob: 0.2946
B-A= 0.25 win_prob: 0.2891
B-A= 0.3 win_prob: 0.32
B-A= 0.35 win_prob: 0.3334
B-A= 0.4 win_prob: 0.3244
B-A= 0.45 win_prob: 0.327
B-A= 0.5 win_prob: 0.3261
B-A= 0.55 win_prob: 0.3328
B-A= 0.6 win_prob: 0.3596
B-A= 0.65 win_prob: 0.3568
B-A= 0.7 win_prob: 0.3638
B-A= 0.75 win_prob: 0.3551
B-A= 0.8 win_prob: 0.3737
B-A= 0.85 win_prob: 0.3613
B-A= 0.9 win_prob: 0.3609
B-A= 0.95 win_prob: 0.3641

A= 0.9
-----------------------------
B-A= 0.05 win_prob: 0.1117
B-A= 0.1 win_prob: 0.2123
B-A= 0.15 win_prob: 0.272
B-A= 0.2 win_prob: 0.2781
B-A= 0.25 win_prob: 0.2822
B-A= 0.3 win_prob: 0.3223
B-A= 0.35 win_prob: 0.3228
B-A= 0.4 win_prob: 0.3297
B-A= 0.45 win_prob: 0.3283
B-A= 0.5 win_prob: 0.3228
B-A= 0.55 win_prob: 0.3262
B-A= 0.6 win_prob: 0.3555
B-A= 0.65 win_prob: 0.3588
B-A= 0.7 win_prob: 0.3611
B-A= 0.75 win_prob: 0.3557
B-A= 0.8 win_prob: 0.3703
B-A= 0.85 win_prob: 0.3624
B-A= 0.9 win_prob: 0.3562
B-A= 0.95 win_prob: 0.3568

A= 0.95
-----------------------------
B-A= 0.05 win_prob: 0.109
B-A= 0.1 win_prob: 0.2144
B-A= 0.15 win_prob: 0.2619
B-A= 0.2 win_prob: 0.2914
B-A= 0.25 win_prob: 0.2935
B-A= 0.3 win_prob: 0.3282
B-A= 0.35 win_prob: 0.3194
B-A= 0.4 win_prob: 0.3247
B-A= 0.45 win_prob: 0.3205
B-A= 0.5 win_prob: 0.3265
B-A= 0.55 win_prob: 0.3228
B-A= 0.6 win_prob: 0.3603
B-A= 0.65 win_prob: 0.3543
B-A= 0.7 win_prob: 0.3575
B-A= 0.75 win_prob: 0.3619
B-A= 0.8 win_prob: 0.3557
B-A= 0.85 win_prob: 0.3656
B-A= 0.9 win_prob: 0.3664
B-A= 0.95 win_prob: 0.3549
"""