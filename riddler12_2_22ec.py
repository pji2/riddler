def pad(b):
    l=len(b)
    s='0'*(18-l)+str(b)
    return s 
    
def validSeq(numA,numB,s):
    if s.count("0")==numA and s.count("1")==numB:
        return True
    return False
    
def numBuses(s):
    nbuses=1
    for n in range(1,18):
        if s[n]!=s[n-1]:
            nbuses+=1 
    return nbuses
  
totalbuses=0
for num in range(2**18):
    a=bin(num)
    a=a[2:]
    s=pad(a)
    if validSeq(11,7,s):
        totalbuses+=numBuses(s) 
print(totalbuses/31824) #18 choose 11

#Output:
#9.555555555555555