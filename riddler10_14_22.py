import operator as op
from functools import reduce
import math

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom
    
def prob(N):
    return ncr(365, N-1)*math.factorial(N-1)*(N-1)/365**N 
    
expected=0
for n in range(1,367):
    totalprob+=prob(n)
    expected+=n*prob(n)
print(expected)


def TripletProb(N):
    totalp=0
    N=N-1
    for k in range(1,math.floor(N/2)+1):
        val=math.factorial(365)*math.factorial(N)*k/(math.factorial(365+k-N)*math.factorial(N-2*k)*2**k*math.factorial(k)*365**(N+1))
        totalp+=val
    return totalp

E=0
for n in range(1,366):
    E+=TripletProb(n)*n
print("Triplet Prob:", E)

"""Output:
24.61658589459887
Triplet Prob: 88.73891765060493
"""