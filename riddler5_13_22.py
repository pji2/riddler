import numpy as np
from numpy.linalg import inv
mat=np.array([[0,3.0/16,9.0/16,5.0/32,3.0/32],[0,3.0/16,9.0/16,5.0/32,3.0/32],[0,1.0/8,5.0/8,1.0/8,1.0/8],[0,0,0,1,0],[0,0,0,0,1]])
#print(mat)
Q=mat[0:3,0:3]
R=mat[0:3,3:5]
N=inv(np.eye(3)-Q)
#print(N)
B=np.matmul(N,R)
print(B)

"""Output:
[[0.55       0.45      ]
 [0.55       0.45      ]
 [0.51666667 0.48333333]]
"""