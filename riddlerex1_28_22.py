N=100
grid=[[0 for x in range(N)] for y in range(N)]
h=int((N-1)/2)
grid[h][h]=1 
grid[h-1][h]=1 
grid[h+1][h]=1 
grid[h][h-1]=1 
grid[h][h+1]=1 
numcolored=5
neighboroffsets=[[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]]

def isvalid(x,y):
    if x>=0 and x<N and y>=0 and y<N:
        return True
    return False

def threeneighbors(grid,i,j):
    c=0
    for t in neighboroffsets:
        x=i+t[0];y=j+t[1]
        if isvalid(x,y) and grid[x][y]==1:
            c+=1 
    if c>=3:
        return True
    return False
    
def copy(grid):
    return [row[:] for row in grid]

def generation(grid):
    newgrid=copy(grid)
    for i in range(N):
        for j in range(N):
            if grid[i][j]==0 and threeneighbors(grid,i,j):
                newgrid[i][j]=1 
                global numcolored
                numcolored+=1 
    return newgrid
    
for x in range(2,11):
    grid=generation(grid)
    print(x,numcolored)

"""Output:
2 9
3 13
4 21
5 29
6 37
7 49
8 61
9 73
10 89
"""