def factorpairs(N):
    pairs=[]
    for n in range(1,int(N/2)+1):
        if int(float(N)/n)==float(N)/n and n<=N/n:
            p=(n,N/n)
            pairs.append(p)
    return pairs


def validfactor(pairs):
    n=0
    for p in pairs:
        if p[0]==1 and p[1]%2==1:
            n+=1
        elif p[0]%2==1 and p[1]%2==1 and p[0]==p[1]:
            n+=1
        elif p[0]%2==1 and p[1]%2==1:
            n+=2
        elif p[0]==1 and p[1]%2==0:
            n+=0
        elif (p[0]%2==1 and p[1]%2==0) or (p[0]%2==0 and p[1]%2==1):
            n+=1
    return n

for n in range(2,150):
    pairs=factorpairs(n)
    N=validfactor(pairs)
    if N>=3:
        print(n,N)

"""Output:
15 3
21 3
27 3
30 3
33 3
35 3
39 3
42 3
45 5
51 3
54 3
55 3
57 3
60 3
63 5
65 3
66 3
69 3
70 3
75 5
77 3
78 3
81 4
84 3
85 3
87 3
90 5
91 3
93 3
95 3
99 5
102 3
105 7
108 3
110 3
111 3
114 3
115 3
117 5
119 3
120 3
123 3
125 3
126 5
129 3
130 3
132 3
133 3
135 7
138 3
140 3
141 3
143 3
145 3
147 5
"""