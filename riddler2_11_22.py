from ortools.sat.python import cp_model
primes=[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
primedomain=[[x,x] for x in primes]

def SatProgram():
    # Creates the model.
    model = cp_model.CpModel()

    # Creates the variables.
    num_vals = 3
    A = model.NewIntVarFromDomain(cp_model. Domain.FromIntervals(primedomain), 'A')
    B = model.NewIntVarFromDomain(cp_model. Domain.FromIntervals(primedomain), 'B')
    C = model.NewIntVarFromDomain(cp_model. Domain.FromIntervals(primedomain), 'C')
    D = model.NewIntVarFromDomain(cp_model. Domain.FromIntervals(primedomain), 'D')
    E = model.NewIntVarFromDomain(cp_model. Domain.FromIntervals(primedomain), 'E')
    F = model.NewIntVarFromDomain(cp_model. Domain.FromIntervals(primedomain), 'F')
    G = model.NewIntVarFromDomain(cp_model. Domain.FromIntervals(primedomain), 'G')
    H = model.NewIntVarFromDomain(cp_model. Domain.FromIntervals(primedomain), 'H')
                                  
    # Creates the constraints.
    model.AddAllDifferent([A,B,C,D,E,F,G,H])
    model.Add(E+G==C+D)
    model.Add(A+B==F+H)
    model.Add(A+E==D+H)
    model.Add(B+G==C+F)
    model.Add(B+D==E+F)
    model.Add(A+C==G+H)

    # Creates a solver and solves the model.
    solver = cp_model.CpSolver()
    status = solver.Solve(model)

    if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
        print('A = %i' % solver.Value(A))
        print('B = %i' % solver.Value(B))
        print('C = %i' % solver.Value(C))
        print('D = %i' % solver.Value(D))
        print('E = %i' % solver.Value(E))
        print('F = %i' % solver.Value(F))
        print('G = %i' % solver.Value(G))
        print('H = %i' % solver.Value(H))
    else:
        print('No solution found.')


SatProgram()
"""Output:
A = 3
B = 13
C = 19
D = 29
E = 31
F = 11
G = 17
H = 5
"""