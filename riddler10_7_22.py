def prob(m,c):
    if m<0:
        return 0
    if m>9:
        return 0
    if m==0 and c==0:
        return 1
    if m==c:
        return 1
    else:
        return m/c*prob(m-1,c-1) + (1-m/c)*prob(m+1,c-1)

print(prob(0,28))

"""Output:
0.7004899349764069
"""
