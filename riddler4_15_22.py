
def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

def noSquareFactors(n,pf):
    for p in pf:
        if n%(p**2)==0:
            return False
    return True

def OneLessDivideNminus1(n,pf):
    for p in pf:
        if (n-1)%(p-1)!=0:
            return False
    return True
    
digits=[0,1,2,3,4,5,6,7,8,9]
for A in digits:
    for B in digits:
        for C in digits:
            Number = 100000*A+10000*B+1000*C+100*A+10*B+C
            if Number==0:
                continue
            factors=prime_factors(Number)
            if noSquareFactors(Number,factors) and OneLessDivideNminus1(Number,factors):
                print(Number)
"""
Output:
41041
101101
"""