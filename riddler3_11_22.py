from itertools import combinations
Outcomes=[]
for i in range(1,7):
    for j in range(1,7):
        for k in range(1,7):
            for l in range(1,7):
                Outcomes.append((i,j,k,l))
SumMatrix=[[0 for x in range(11)] for y in range(6**4)]

for i in range(len(Outcomes)):
    O=Outcomes[i]
    sums=[O[0]+O[1],O[0]+O[2],O[0]+O[3],O[1]+O[2],O[1]+O[3],O[2]+O[3]]
    sums=set(sums)
    for s in sums:
        SumMatrix[i][s-2]=1


arr=[2,3,4,5,6,7,8,9,10,11,12]
maxSelection=set()
maxScore=0
for c in combinations(arr,4):
    score=0
    for i in range(6**4):
        s=min(1,SumMatrix[i][c[0]-2]+SumMatrix[i][c[1]-2]+SumMatrix[i][c[2]-2]+SumMatrix[i][c[3]-2])
        score+=s
        #print(c,Outcomes[i],s)
    if score>maxScore:
        maxScore=score
        maxSelection=c
print(maxSelection, maxScore)
print(maxScore/6**4)

"""Output:
(4, 6, 8, 10) 1264
0.9753086419753086
"""