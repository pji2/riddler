#Loads the words as sets
with open('riddler9_16_22_wordlist.txt') as f:
    allsets=set()
    nwords=0
    while True:
        line = f.readline()
        #if nwords>1000:
         #   break
        if not line:
            break
        w=line.strip()
        if len(w)<4:
            continue
        w_sorted=''.join(sorted(w))
        #print(w,w_sorted,len(w))
        allsets.add(w_sorted)
        nwords+=1
    #print(len(allsets))
    
#Graph implementation with BFS    
from collections import deque
from queue import Queue

class Node:
    def __init__(self, val, edges=[], visited=False, distance=-1):
        self.val = val
        self.edges = edges
        self.visited = visited
        self.distance = distance

class Graph:
    def __init__(self, nodes=[]):
        self.nodes = nodes

    def add_node(self, val):
        newNode = Node(val,[],False,-1)
        self.nodes.append(newNode)
        return newNode

    def add_edge(self, node1, node2):
        node1.edges.append(node2)
        #node2.edges.append(node1)
    
    def set_nodes_unvisited(self):
        for n in self.nodes:
            n.visited=False
    
    def set_nodes_nodistance(self):
        for n in self.nodes:
            n.distance=-1
        
    def BFS(self, n):
        # marking all nodes as unvisited
        self.set_nodes_unvisited()
        # mark all distance with -1
        self.set_nodes_nodistance()
 
        # distance of u from u will be 0
        n.distance=0
        # in-built library for queue which performs fast operations on both the ends
        queue = deque()
        queue.append(n)
        # mark node u as visited
        n.visited = True
 
        while queue:
 
            # pop the front of the queue(0th element)
            front = queue.popleft()
            # loop for all adjacent nodes of node front
 
            for e in front.edges:
                if not e.visited:
                    # mark the ith node as visited
                    e.visited = True
                    # make distance of i , one more than distance of front
                    e.distance = front.distance+1
                    # Push node into the stack only if it is not visited already
                    queue.append(e)
 
        maxDis = 0
        nodeIdx = ""
 
        # get farthest node distance and its index
        for n in self.nodes:
            if n.distance > maxDis:
 
                maxDis = n.distance
                nodeIdx = n
 
        return nodeIdx, maxDis

    def BFS_Path(self, start_node, target_node):
        # Set of visited nodes to prevent loops
        visited = set()
        queue = Queue()

        # Add the start_node to the queue and visited list
        queue.put(start_node)
        visited.add(start_node)

        # start_node has not parents
        parent = dict()
        parent[start_node] = None

        # Perform step 3
        path_found = False
        while not queue.empty():
            current_node = queue.get()
            if current_node == target_node:
                path_found = True
                break

            for next_node in current_node.edges:
                if next_node not in visited:
                    queue.put(next_node)
                    parent[next_node] = current_node
                    visited.add(next_node)

        # Path reconstruction
        path = []
        if path_found:
            path.append(target_node)
            while parent[target_node] is not None:
                path.append(parent[target_node]) 
                target_node = parent[target_node]
            path.reverse()
        return path 

#Sort word sets by length to generate tree
wordlendict=dict()
WordGraph=Graph([])
for a in allsets:
    length=len(a)
    N = WordGraph.add_node(a)
    if length not in wordlendict:
        wordlendict[length]=[N]
    else:
        wordlendict[length].append(N)
        
#for l in wordlendict:
#    print(l,len(wordlendict[l]))
#print(len(WordGraph.nodes))

#Find the words to connect. Words are length 4-25 (No words are length 26)
def SingleDifference(s, t):  
    ls_s = [s[i] for i in range(len(s))]  
    ls_t = [t[i] for i in range(len(t))]  
    for elem in ls_s:  
        if elem in ls_t:
            ls_t.remove(elem)  
        else:
            return False
    if len(ls_t)==1:
        return True
    return False

for wl in range(4,25):
    #print(wl)
    first_level=wordlendict[wl]
    second_level=wordlendict[wl+1]
    for f in first_level:
        for s in second_level:
            #print(f.val,s.val)
            if SingleDifference(f.val,s.val):
                WordGraph.add_edge(f,s)

#Find max distance
maxdist=0
maxinit=""
maxwordset=""
for word in wordlendict[4]:
    w,d = WordGraph.BFS(word)
    if d>maxdist:
        maxdist=d
        maxwordset=w
        maxinit=word
print(maxdist+1,maxinit.val,maxwordset.val)

path=WordGraph.BFS_Path(maxinit,maxwordset)
for p in path:
    print(p.val)

#Print the words to show added letter at end
Final_Wordlist=["aemn","aeimn","aeimns","aeimnos","aeimnost","aeimnnost","aeiimnnost","aeiimnnorst","aeiimnnorstt","adeiimnnorstt","adeeiimnnorstt","adeeiimnnorsttu","adeeiimnnorssttu"]
print(Final_Wordlist[0])
curr_str=""
for w_i in range(1,len(Final_Wordlist)):
    curr_word=Final_Wordlist[w_i]
    for letter in Final_Wordlist[w_i-1]:
        curr_word=curr_word.replace(letter,'',1)
    curr_str+=curr_word
    print(Final_Wordlist[0]+curr_str)

#Show the dictionary words each word set matches to
Final_Wordlist_2=["aemn","aemni","aemnis","aemniso","aemnisot","aemnisotn","aemnisotni","aemnisotnir","aemnisotnirt","aemnisotnirtd","aemnisotnirtde","aemnisotnirtdeu","aemnisotnirtdeus"]
solution=[]
with open('riddler9_16_22_wordlist.txt') as AllWords:
    while True:
        line = AllWords.readline()
        #if nwords>1000:
         #   break
        if not line:
            break
        w=line.strip()
        if len(w)<4:
            continue
        w_sorted=''.join(sorted(w))
        for wf in Final_Wordlist_2:
            wfcomp=''.join(sorted(wf))
            if w_sorted == wfcomp:
                i=Final_Wordlist_2.index(wf)
                solution.append((len(w),Final_Wordlist_2[i],w))
            
sol=sorted(solution,key=lambda x: x[0])
for s_ in sol:
    print(s_)

"""Output:
13 aemn adeeiimnnorssttu
aemn
aemni
aemnis
aemniso
aemnisot
aemnisotn
aemnisotni
aemnisotnir
aemnisotnirt
aemnisotnirtd
aemnisotnirtde
aemnisotnirtdeu
aemnisotnirtdeus

(4, 'aemn', 'amen')
(4, 'aemn', 'mane')
(4, 'aemn', 'mean')
(4, 'aemn', 'name')
(4, 'aemn', 'nema')
(5, 'aemni', 'amine')
(5, 'aemni', 'anime')
(5, 'aemni', 'minae')
(6, 'aemnis', 'amines')
(6, 'aemnis', 'animes')
(6, 'aemnis', 'inseam')
(6, 'aemnis', 'mesian')
(6, 'aemnis', 'semina')
(7, 'aemniso', 'anomies')
(8, 'aemnisot', 'amniotes')
(8, 'aemnisot', 'misatone')
(9, 'aemnisotn', 'nominates')
(10, 'aemnisotni', 'antimonies')
(10, 'aemnisotni', 'antinomies')
(11, 'aemnisotnir', 'inseminator')
(11, 'aemnisotnir', 'nitrosamine')
(12, 'aemnisotnirt', 'terminations')
(13, 'aemnisotnirtd', 'antimodernist')
(14, 'aemnisotnirtde', 'determinations')
(15, 'aemnisotnirtdeu', 'underestimation')
(16, 'aemnisotnirtdeus', 'underestimations')
"""