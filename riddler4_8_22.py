Ainit=6
Binit=8
Cinit=10
NashmatrixB=[[0 for y in range(11)] for x in range(11)]
NashmatrixC=[[0 for y in range(11)] for x in range(11)]

#returns payoff for b,c
def Victory(ls):
    a=ls[0];b=ls[1];c=ls[2]
    if (b<a and a<c) or (c<a and a<b):
        return [1,-0.5,-0.5]
    elif (a<b and b<c) or (c<b and b<a):
        return [-0.5,1,-0.5]
    elif (a<c and c<b) or (b<c and c<a):
        return [-0.5,-0.5,1]
    elif a==b and b==c:
        return [0,0,0]
    elif a==b:
        return [0.5,0.5,-1]
    elif b==c:
        return [-1,0.5,0.5]
    elif a==c:
        return [0.5,-1,0.5]
    else:
        print("error")

#calculates final score
def Additionals(ai,bi,ci,a,b,c):
    if (b<a and a<c) or (c<a and a<b):
        return Victory([ai+a,bi,ci])
    elif (a<b and b<c) or (c<b and b<a):
        return Victory([ai,bi+b,ci])
    elif (a<c and c<b) or (b<c and c<a):
        return Victory([ai,bi,ci+c])
    elif a==b and b==c:
        v1=Victory([ai+a,bi,ci])
        v2=Victory([ai,bi+b,ci])
        v3=Victory([ai,bi,ci+c])
        return [(v1[0]+v2[0]+v3[0])/3.0,(v1[1]+v2[1]+v3[1])/3.0,(v1[2]+v2[2]+v3[2])/3.0]
    elif a==b:
        v1=Victory([ai+a,bi,ci])
        v2=Victory([ai,bi+b,ci])
        return [(v1[0]+v2[0])/2.0,(v1[1]+v2[1])/2.0,(v1[2]+v2[2])/2.0]
    elif b==c:
        v2=Victory([ai,bi+b,ci])
        v3=Victory([ai,bi,ci+c])
        return [(v2[0]+v3[0])/2.0,(v2[1]+v3[1])/2.0,(v2[2]+v3[2])/2.0]
    elif a==c:
        v1=Victory([ai+a,bi,ci])
        v3=Victory([ai,bi,ci+c])
        return [(v1[0]+v3[0])/2.0,(v1[1]+v3[1])/2.0,(v1[2]+v3[2])/2.0]
    else:
        print("error")
    
    
for b in range(11):
    for c in range(11):
        payoff=Additionals(Ainit,Binit,Cinit,3,b,c)
        NashmatrixB[b][c]=payoff[1]
        NashmatrixC[b][c]=payoff[2]
        
for b in range(11):
    for c in range(11):
        optimalforc=max(NashmatrixC[b])
        optimalforb=max([NashmatrixB[i][c] for i in range(11)])
        if NashmatrixB[b][c]==optimalforb and NashmatrixC[b][c]==optimalforc:
            print("Nash equilibrium at",b,c,"payoffs",NashmatrixB[b][c],NashmatrixC[b][c])

"""Output:
Nash equilibrium at 0 0 payoffs 1.0 -0.5
Nash equilibrium at 0 1 payoffs 1 -0.5
Nash equilibrium at 0 2 payoffs 1 -0.5
Nash equilibrium at 0 3 payoffs 0.25 -0.5
Nash equilibrium at 1 0 payoffs 1 -0.5
Nash equilibrium at 1 1 payoffs 1.0 -0.5
Nash equilibrium at 1 2 payoffs 1 -0.5
Nash equilibrium at 1 3 payoffs 0.25 -0.5
Nash equilibrium at 10 10 payoffs 0.25 0.25
"""