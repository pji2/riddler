import matplotlib.pyplot as plt
import operator as op
from functools import reduce

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom  # or / in Python 2

maxN=1000
Pstored=[-1 for i in range(maxN+1)]
Pstored[0]=0; Pstored[1]=1
for p in range(2,maxN+1):
    Pstored[p]=0
    s=0
    for i in range(1,p+1):
        s+=ncr(p,i)*Pstored[p-i]
    s/=(2**p-1)
    Pstored[p]=s
print(Pstored[-1])

plt.plot([x for x in range(20,1000)],Pstored[20:1000])
plt.show()

"""Output:
0.7213533784217128
"""
