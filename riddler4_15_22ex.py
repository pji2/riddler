dictionary=dict()
with open("Syllables.txt") as f:
    for line in f:
        c+=1
        syll=line.split("=")[1]
        word=line.split("=")[0]
        syllables=line.count("·")+1
        dictionary[word]=syllables
vowels=['a','e','i','o','u']
for word in dictionary:
    for l in range(len(word)):
        if word[l] in vowels:
            word_=word[:l]+word[l+1:]
            if word_ in dictionary:
                if dictionary[word_]==dictionary[word]+1:
                    print(word_,word)

"""Output:
id aid
am aim
bar bare
bar bear
beatified beautified
beatify beautify
bar boar
brassier brassiere
cation caution
col coal
col coil
col cool
col cool
cop cope
coup coupe
cursed coursed
dam dame
duce deuce
dun dune
col ecol
expos expose
gal gale
gal goal
lam lame
leu lieu
lam loam
lope loupe
par pair
par pare
pace peace
par pear
ruse rouse
sal sail
sal sale
sal seal
sine seine
vice voice
yen yean
"""