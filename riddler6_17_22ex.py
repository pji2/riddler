#Limitation to changing 1 digit

def method1(N): #make the value divisible by 11 by either subtracting x from an even placed digit, 
    #or adding x to an odd placed digit
    x=N%11
    N_str=str(N)
    N_r=N_str[::-1]
    N_r_new=N_r[:]
    for d_i in range(len(N_r)):
        d=N_r[d_i]
        if d_i%2==0:
            if int(d)>=x: #can subtract whole of x from digit
                new_x=int(d)-x
                N_r_new=N_r_new[:d_i]+str(new_x)+N_r_new[d_i+1:]
                break
        else:
            if 9-int(d)>=x: #can add whole of x
                new_x=int(d)+x
                N_r_new=N_r_new[:d_i]+str(new_x)+N_r_new[d_i+1:]
                break
        if d_i==len(N_r)-1 and x>0:
            return -1
    return N_r_new[::-1]

def method2(N): #we can add 11-x to an even placed digit, 
    #or subtract 11-x from an odd placed digit
    x=11-N%11
    N_str=str(N)
    N_r=N_str[::-1]
    N_r_new=N_r[:]
    for d_i in range(len(N_r)):
        d=N_r[d_i]
        if d_i%2==1:
            if int(d)>=x: #can subtract whole of x from digit
                new_x=int(d)-x
                N_r_new=N_r_new[:d_i]+str(new_x)+N_r_new[d_i+1:]
                break
        else:
            if 9-int(d)>=x: #can add whole of x
                new_x=int(d)+x
                N_r_new=N_r_new[:d_i]+str(new_x)+N_r_new[d_i+1:]
                break
        if d_i==len(N_r)-1 and x>0:
            return -1
    return N_r_new[::-1]
    

for i in range(1,1000):
    if method1(i)==-1 and method2(i)==-1:
        print(i)

"""Output:
545
"""