from scipy import integrate

def func(a,b,c,d):
    return a/(a+d)*b/(b+c)*(a/(a+b)*a+b/(a+b)*b) + a/(a+d)*c/(b+c)*(a/(a+c)*a+c/(a+c)*c) + d/(a+d)*b/(b+c)*(d/(b+d)*d+b/(b+d)*b) +\
d/(a+d)*c/(b+c)*(d/(c+d)*d+c/(c+d)*c)

def lim0(b,c,d):
    return [0,b]
    
def lim1(c,d):
    return [0,c]

def lim2(d):
    return [0,d] 
    
def lim3():
    return [0,1] 
        

    
val=integrate.nquad(func,[lim0,lim1,lim2,lim3])
print(val[0]*24)

###Output:
#0.6735417813867959
