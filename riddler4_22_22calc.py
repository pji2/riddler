import numpy as np
markovmat=[[0 for x in range(6)] for y in range(6)]
probs=[1.0,0.8,0.6,0.4,0.2]
markovmat[0][1]=1
for i in range(1,5):
    markovmat[i][i+1]=probs[i]
    markovmat[i][i-1]=1.0-probs[i]
markovmat[5][5]=1 

Q=[[m[i] for i in range(5)] for m in markovmat[0:5]]
I=np.identity(5)
M=np.subtract(I,Q)
N=np.linalg.inv(M)
print(sum(N[0]))

#Output:
#42.66667