#Simulate probability of winning on first 5 cards given that you do not win on first 4 cards

import random

def valid4(nums):
    if 1 in nums and 2 in nums and 3 in nums and 4 in nums:
        return True
    return False

def valid5(nums):
    if 2 in nums and 3 in nums and 4 in nums and 5 in nums:
        return True
    if 1 in nums and 3 in nums and 4 in nums and 5 in nums:
        return True
    if 1 in nums and 2 in nums and 4 in nums and 5 in nums:
        return True
    if 1 in nums and 2 in nums and 3 in nums and 5 in nums:
        return True
    if 1 in nums and 2 in nums and 3 in nums and 4 in nums:
        return True
    return False
    

deck=[x for x in range(1,55)]
n4=0
n5=0
for i in range(1000000):
    random.shuffle(deck)
    board=deck[:16]
    TLcorner=[0,1,2,4,5,6,8,9,10]
    foundset4=False
    foundset5=False
    for ind in TLcorner:
        setoffour=[board[ind],board[ind+1],board[ind+4],board[ind+5]]
        if valid4(setoffour):
            foundset4=True
        if valid5(setoffour):
            foundset5=True
    Rows=[0,4,8,12]
    for r in Rows:
        row=[board[r],board[r+1],board[r+2],board[r+3]]
        if valid4(row):
            foundset4=True
        if valid5(row):
            foundset5=True
    Columns=[0,1,2,3]
    for r in Columns:
        col=[board[r],board[r+4],board[r+8],board[r+12]]
        if valid4(col):
            foundset4=True
        if valid5(col):
            foundset5=True
    corners=[board[0],board[3],board[12],board[15]]
    if valid4(col):
        foundset4=True
    if valid5(col):
        foundset5=True
    if not foundset4:
        n4+=1 
    if not foundset4 and foundset5:
        n5+=1
print("Prob board wins on 5th card but not on first 4:", n5/n4)
print("Prob exactly one winner:", 1000*(1-n5/n4)**999*(n5/n4))


#Output:
#Prob board wins on 5th card but not on first 4: 0.00021001092056786952
#Prob exactly one winner: 0.17026168705346928

#Analytical result: 8/35137 = 0.00022768
#Prob exactly one winner: 0.181356
