import numpy as np

def MarkovMat(x):
    states=[]
    for n in range(-100,101):
        states.append(n)
    MarkovMat=[[0 for y in range(201)] for x in range(201)]
    for score_diff_from in range(201):
        for delta in range(-1,2):
            from_state=states[score_diff_from]
            if (score_diff_from==0 and delta==-1) or (score_diff_from==200 and delta==1):
                continue
            if from_state==0 and delta==0:
                MarkovMat[score_diff_from][score_diff_from]=0.5**2+0.5*(0.5+x)
            elif from_state==0 and delta==1:
                MarkovMat[score_diff_from][score_diff_from+1]=0.5*(0.5-x)
            elif from_state==0 and delta==-1:
                MarkovMat[score_diff_from][score_diff_from-1]=0.5**2
                
            elif from_state>0 and delta==0:
                MarkovMat[score_diff_from][score_diff_from]=(0.5+x)*(0.5-x)*2
            elif from_state>0 and delta==-1:
                MarkovMat[score_diff_from][score_diff_from-1]=(0.5+x)**2
            elif from_state>0 and delta==1:
                MarkovMat[score_diff_from][score_diff_from+1]=(0.5-x)**2   
                
            elif from_state==-1 and delta==0:
                MarkovMat[score_diff_from][score_diff_from]=(0.5-x)*(0.5+x)+(0.5+x)*0.5
            elif from_state==-1 and delta==1:
                MarkovMat[score_diff_from][score_diff_from+1]=(0.5+x)*0.5
                
            elif from_state<0 and delta==0:
                MarkovMat[score_diff_from][score_diff_from]=(0.5-x)*(0.5+x)*2
            elif from_state<0 and delta==-1:
                MarkovMat[score_diff_from][score_diff_from-1]=(0.5-x)**2
            elif from_state<0 and delta==1:
                MarkovMat[score_diff_from][score_diff_from+1]=(0.5+x)**2
    return np.array(MarkovMat)

def endingDistribution(x):
    m=MarkovMat(x)
    o=np.matmul(m,m)
    for n in range(98):
        o=np.matmul(m,o)
    init=np.array([0 for x in range(201)])
    init[100]=1
    res=np.matmul(init,o)
    return res
ED = endingDistribution(0.25)
print(ED[100])
print(sum(ED[0:100]),sum(ED[101:]))

"""Output:
0.5000000000000003
0.3749999999999994 0.12500000000000025
"""