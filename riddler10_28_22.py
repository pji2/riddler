valdict=dict()
def E(n,pmin,pmax):
    if (n,pmin,pmax) in valdict:
        return valdict[(n,pmin,pmax)]
    if n>=0 and pmin==0 and pmax==0:
        return n 
    elif n==0:
        return (pmin+pmax)/2
    elif n>0 and pmin==0:
        min_p=1000
        c_choice=0
        for c in range(1,min(n+1,4)):
            if c>n:
                break
            val=E(n-c,0,pmax-1)
            if val<min_p:
                min_p=val 
                c_choice=c
        res = 1/(pmax+1)*n+pmax/(pmax+1)*E(n-c_choice,0,pmax-1)
        if (n,pmin,pmax) not in valdict:
            valdict[(n,pmin,pmax)]=res
        return res
    else:
        min_p=1000
        c_choice=0
        for c in range(1,min(n+1,4)):
            if c>n:
                break
            val=E(n-c,pmin-1,pmax-1)
            if val<min_p:
                min_p=val 
                c_choice=c
        res = E(n-c_choice,pmin-1,pmax-1)
        if (n,pmin,pmax) not in valdict:
            valdict[(n,pmin,pmax)]=res
        return res
print(E(150,50,150))

#Output:
#25.247524752475247
