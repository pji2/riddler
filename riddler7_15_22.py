
from itertools import product

#first index of matrix are following transformations: about x-axis 90 degrees, about z-axis 90 degrees
#second index of matrix are the following faces: T,L,R,F,Ba,Bo (Top, left, right, front, back, bottom)
#first in each tuple pair is the previous face # the current face derives from, second in pair indicates if the mark 
#orientation changes or not
transformation_1=[[(3,0),(1,1),(2,1),(5,0),(0,0),(4,0)],[(0,1),(3,0),(4,0),(2,0),(1,0),(5,1)]]

def rotatecube(state,rotate_type):
    new_b=["","","","","",""]
    #print("in rotatecube:",state,rotate_type)
    for b_i in range(6):
        t_code=transformation_1[rotate_type][b_i]
        index_from=t_code[0]
        new=(state[index_from]+t_code[1])%2
        ##EC: new=(state[index_from]+t_code[1]*2)%4
        new_b[b_i]=new
    #print(new_b)
    return new_b
    
def rotateMarking(b,unique_faces):
    new_markings=[]
    for i in range(4):
        for j in range(4):
            b=rotatecube(b,0)
            new_markings.append(b)
        b=rotatecube(b,1)
    b=rotatecube(rotatecube(b,0),1)
    for i in range(4):
        b=rotatecube(b,0)
        new_markings.append(b)
    b=rotatecube(rotatecube(b,1),1)
    for i in range(4):
        b=rotatecube(b,0)
        new_markings.append(b)
    for n in new_markings:
        if n in unique_faces:
            return False
    return True
        
unique_faces=[]
unique_marks=0
for b in product([0,1],repeat=6):
##EC: for b in product([0,1,2,3],repeat=6):
    found_same_markings=rotateMarking(b,unique_faces)
    if found_same_markings:
        unique_marks+=1
        unique_faces.append(list(b))
        
print(unique_marks)
print(unique_faces)

"""Output:
8
[[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 1, 1], [0, 0, 0, 1, 1, 0], [0, 0, 1, 0, 0, 0], [0, 0, 1, 0, 1, 1], [0, 0, 1, 1, 0, 1]]
"""