import itertools
import collections
import numpy as np


def flatten(x):
    if isinstance(x, collections.Iterable):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]

def VectorCombinations(n):
    combs = [item for item in itertools.product([-1,0,1], repeat=n)]
    combs.remove(tuple([0 for i in range(n)]))
    return combs

N=3
BlockCount = dict()
coords=itertools.product([x for x in range(3)], repeat=N)
for c in coords:
    BlockCount[c]=0
#print(BlockCount)

#Make a line with this vector and starting position, and keep track of how many blocks we go through
def count_lines(vector,startpos):
    global BlockCount
    #print(vector,startpos)
    BlockCount[tuple(startpos)]+=1
    for i in range(1,3):
        newp=startpos+i*np.array(vector)
        BlockCount[tuple(newp)]+=1
        
"""
if the displacement vector is v_i=-1, then the starting coordinate s_i=2 on a tic tac toe board
if the displacement vector is v_i=0, then the starting coordinate s_i=0 or 1 or 2 on a tic tac toe board
if the displacement vector is v_i=1, then the starting coordinate s_i=0 on a tic tac toe board
"""
VectorDict = {-1:[2],0:[0,1,2],1:[0]}

Vs = VectorCombinations(N)
#print(Vs)
for v in Vs:
    s=[]
    for vi in v:
        s.append(VectorDict[vi])
    #print("vector:",v,s)
    allstarts=s[0]
    j=1
    for i in range(1,len(s)):
        sj=s[j]
        allstarts=itertools.product(allstarts,sj)
        allstarts=list(allstarts)
        j+=1
    for a in allstarts:
        count_lines(v,flatten(a))
#print(BlockCount) #note that block counts are double-counted, since we have a positive and negative vector
minkey=(0,0,0)
maxkey=(0,0,0)
maxval=0
minval=100
for key in BlockCount:
    if BlockCount[key]<minval:
        minval=BlockCount[key]
        minkey=key
    if BlockCount[key]>maxval:
        maxval=BlockCount[key]
        maxkey=key
print(minkey,minval/2.0,maxkey,maxval/2.0)

"""Output:
(0, 0, 1) 4.0 (1, 1, 1) 13.0
"""