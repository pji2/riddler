import random
import matplotlib.pyplot as plt

def try_knock_pin(p):
    p_try=random.random()
    if p_try<=p:
        return True
    return False

counts=0
for n in range(10000):
    pins_down=[1]
    max_row=100
    knock_prob=0.7
    for row in range(2,max_row+1):
        new_pins_down=[0]*row
        for i in range(len(pins_down)):
            if pins_down[i]==1:
                if try_knock_pin(knock_prob):
                    new_pins_down[i]=1 
                if try_knock_pin(knock_prob):
                    new_pins_down[i+1]=1 
        pins_down=new_pins_down[:]

    for row in range(max_row-1,0,-1):
        new_pins_down=[0]*row 
        for i in range(len(pins_down)):
            if pins_down[i]==1:
                if i==0:
                    if try_knock_pin(knock_prob):
                        new_pins_down[i]=1 
                elif i==len(pins_down)-1:
                    if try_knock_pin(knock_prob):
                        new_pins_down[i-1]=1 
                else:
                    if try_knock_pin(knock_prob):
                        new_pins_down[i]=1 
                    if try_knock_pin(knock_prob):
                        new_pins_down[i-1]=1 
        pins_down=new_pins_down[:]     
    counts+=pins_down[0]
print(counts/10000)

"""Output:
0.5815
"""