import csv, itertools

def getprimeslengthN(n):
    l=[]
    with open('P-10000.txt', 'r') as file:
        reader = csv.reader(file)
        for row in reader:
            prime=row[1]
            prime=prime[1:]
            if len(prime)==n:
                l.append(int(prime))
    return l

def diff(p,q):
    numdiffs=0
    p=str(p);q=str(q)
    for i in range(len(str(p))):
        if p[i]!=q[i]:
            numdiffs+=1
    if numdiffs==1:
        return True
    else:
        return False

def buildGraph(n):
    graph={}
    PrimeList=getprimeslengthN(n)
    for p in range(len(PrimeList)):
        for q in range(p+1,len(PrimeList)):
            firstp=PrimeList[p]
            secondp=PrimeList[q]
            if diff(firstp,secondp):
                if firstp not in graph:
                    graph[firstp]=[secondp]
                else:
                    graph[firstp].append(secondp)
                if secondp not in graph:
                    graph[secondp]=[firstp]
                else:
                    graph[secondp].append(firstp)
    return graph


def bidirectionalsearch(graph, start, goal):
    current_vertices=[start,goal]
    visited_vertices=set()
    path_dict={start:[start],goal:[goal]}
    if start==goal:
        return start
    while len(path_dict)>0:
        current_vertices_copy=current_vertices[:]
        for vertex in current_vertices_copy:
            origin_=path_dict[vertex]
            origin=origin_[0] #start point
            adjacents=set(graph[vertex]) - visited_vertices
            meeting_points=adjacents.intersection(current_vertices_copy)
            if len(meeting_points)>0:
                for m in meeting_points:
                    if origin!=path_dict[m][0]:
                        pr=path_dict[m]
                        pr.reverse()
                        return path_dict[vertex]+pr
            #Reached a leaf
            if len(set(adjacents)-visited_vertices-set(current_vertices_copy))==0:
                current_vertices.remove(vertex)
                path_dict.pop(vertex,None)
                visited_vertices.add(vertex)
            else:
                for a in adjacents-visited_vertices-set(current_vertices_copy):
                    newpath=path_dict[vertex]
                    path_dict[a]=newpath+[a]
                    current_vertices_copy.append(a)
                    current_vertices.append(a)
                visited_vertices.add(vertex)
                path_dict.pop(vertex,None)
                current_vertices.remove(vertex)
    return None
    
primes=getprimeslengthN(4)
print(len(primes))
g=buildGraph(4)
longestseq=[]
length=0
for comb in itertools.combinations(primes,2):
    ladder = bidirectionalsearch(g,comb[0],comb[1])
    if len(ladder)>length:
        length=len(ladder)
        longestseq=ladder
print(length,longestseq)


"""Output:
1061
9 [9199, 9109, 3109, 3169, 3469, 3449, 5449, 5441, 2441]
"""