###submission
#First guess: trace
#Second guess: Varies depending on response
#Third guess: guess mystery word randomly

import csv
mystery_dictionary=[]
lines=0
with open('riddler1_24_22mysterywords.csv') as file:
    reader=csv.reader(file)
    for row in reader:
        lines+=1
        mystery_dictionary.append(row[0])

guess_dictionary=[]
lines=0
with open('riddler1_24_22guessablewords.csv') as file:
    reader=csv.reader(file)
    for row in reader:
        lines+=1
        guess_dictionary.append(row[0])
        
def lettermatch(proposal,mysteryword):
    matchinds=[]
    comp=""
    for i in range(len(proposal)):
        if proposal[i]==mysteryword[i]:
            matchinds.append(i)
            comp+="!"
        else:
            comp+=proposal[i]
    return matchinds,comp

def hasletter(proposal,mystery,matchinds,comp):
    hasinds=[]
    for i in range(len(proposal)):
        if i not in matchinds and mystery[i] in comp:
            compind=comp.index(mystery[i])
            hasinds.append(compind)
            comp=comp[:compind]+"?"+comp[compind+1:]
    return sorted(hasinds)

def includedletters(proposal,matchinds,hasinds,goal):
    for i in range(5):
        if i in matchinds or i in hasinds:
            includedl.append(proposal[i])
    return includedl

def getmatchcode(matches,present):
    wordcode=""
    for i in range(5):
        if i in matches:
            wordcode+="1"
        elif i in present:
            wordcode+="2"
        else:
            wordcode+="0"
    return wordcode

def resultcode(proposal,goal):
    matches,comp=lettermatch(proposal,goal)
    present=hasletter(proposal,goal,matches,comp)
    return getmatchcode(matches,present)

def possibleoutputs(proposal,dic):
    codewordfreq={}
    for d in dic:
        code=resultcode(proposal,d)
        if code not in codewordfreq:
            codewordfreq[code]=[d]
        else:
            codewordfreq[code].append(d)
    return codewordfreq

#the total number of words possible depending on the response
def distribution(mysterywords,guesswords):
    distribution={}
    for gw in guesswords:
        codewords=possibleoutputs(gw,mysterywords)
        distribution[gw]=len(codewords)
        #print(gw,len(codewords))
    return distribution

#pick first round guess that maximizes the support of the distribution
distributionresult=distribution(mystery_dictionary,guess_dictionary)
maxbuckets=0
maxword=""
for w in distributionresult:
    if distributionresult[w]>maxbuckets:
        maxbuckets=distributionresult[w]
        maxword=w
print(maxword)

#If the response to second to last guess is XXXXX, and there are n possible words remaining,
#the chance of winning is 1/n*n. The total number of winnable words = total number of responses.
def ncorrect(freq):
    return len(freq)
    

#calculate the best guess for second round before we make our final choice of mystery word
#best word varies depending on outcome of first guess
def optimizeguess(guess_dictionary,codewordfreq,level):
    numcorrect=0
    for code in codewordfreq:
        maxn=0
        #Get the best next guess word for this response code
        for w in guess_dictionary:
            newfreq=possibleoutputs(w,codewordfreq[code])
            if level==1:
                n=ncorrect(newfreq)
            else:
                n=optimizeguess(guess_dictionary,newfreq,level-1)
            #print(newfreq,w,n)
            if n>maxn:
                maxn=n
        numcorrect+=maxn
    return numcorrect

result=possibleoutputs("trace",mystery_dictionary)
o=optimizeguess(guess_dictionary,result,1) #Using TRACE as first guess
print(o)
print("win probability", o/len(mystery_dictionary))

"""Output:
trace
1388
win probability 0.5995680345572354
"""